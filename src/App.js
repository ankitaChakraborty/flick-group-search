import React, { Component } from "react";
import "./App.css";
import Flicker from "./components/Flickr";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Flicker />
      </div>
    );
  }
}

export default App;
