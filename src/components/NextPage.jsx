import React, { Component } from "react";
class NextPage extends Component {
  state = {};
  render() {
    return (
      <div className="next-page-div" style={this.props.nextPageStyle}>
        <div class="prev-btn">
          <button
            disabled={this.props.page === 1 ? true : false}
            value="0"
            className="btn btn-primary"
            onClick={this.props.prevNextPage}
          >
            &#60;&#60; Previous
          </button>
        </div>
        <div class="next-btn">
          <button
            value="1"
            onClick={this.props.prevNextPage}
            className="btn btn-primary"
          >
            Next >>
          </button>
        </div>
      </div>
    );
  }
}

export default NextPage;
