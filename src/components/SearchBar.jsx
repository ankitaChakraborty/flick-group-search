import React, { Component } from "react";
class SearchBar extends Component {
  state = {};

  render() {
    return (
      <div className="input-box">
        <div className="input">
          <input
            placeholder="Search for groups.."
            className="form-control"
            type="text"
            value={this.props.input}
            onChange={this.props.change}
            autoFocus
          />
        </div>
        <div className="button">
          <button className="btn btn-primary" onClick={this.props.search}>
            <i class="fas fa-search" />
          </button>
        </div>
      </div>
    );
  }
}

export default SearchBar;
