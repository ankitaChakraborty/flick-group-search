import React, { Component } from "react";
class Display extends Component {
  state = {};
  render() {
    return (
      <div
        className="result-card"
        data-toggle="tooltip"
        title={this.props.group.name}
        data-placement="left"
      >
        <div className="image">
          <img
            src={
              "https://farm" +
              this.props.group.iconfarm +
              ".staticflickr.com/" +
              this.props.group.iconserver +
              "/buddyicons/" +
              this.props.group.nsid +
              ".jpg"
            }
          />
        </div>
        <div className="result-details">
          <div className="result-name">
            <span className="group-name">{this.props.group.name}</span>
          </div>
          <div className="result-numbers">
            <div className="member-count">
              <i class="fa fa-users" aria-hidden="true" />
              {this.props.group.members}
            </div>
            <div className="pool-count">
              <i class="fas fa-images" />
              {this.props.group.pool_count}
            </div>
            <div className="comment-count">
              <i class="fas fa-comments" />
              {this.props.group.topic_count}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Display;
