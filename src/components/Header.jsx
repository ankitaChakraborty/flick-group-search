import React, { Component } from "react";
class Header extends Component {
  render() {
    return (
      <div className="header-div">
        <h1 class="text-primary">Search for Flickr Groups</h1>
      </div>
    );
  }
}

export default Header;
