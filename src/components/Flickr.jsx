import React, { Component } from "react";
import SearchBar from "./SearchBar";
import Display from "./Display";
import Header from "./Header";
import Nextpage from "./NextPage";
class Flickr extends Component {
  state = {
    input: "",
    data: [],
    perPage: 28,
    page: 1,
    nextPageStyle: { visibility: "hidden" }
  };

  prevNextPage = e => {
    if (e.target.value == 0 && this.state.page === 1) {
    } else {
      console.log("else");
      this.setState({
        page: e.target.value == 1 ? ++this.state.page : --this.state.page
      });
      const url =
        "https://api.flickr.com/services/rest/?&method=flickr.groups.search&api_key=d5664b7cbf1eb574ebfa9b774697210a&text=" +
        this.state.input +
        "&per_page=" +
        this.state.perPage +
        "&page=" +
        this.state.page +
        "&format=json&nojsoncallback=1";
      fetch(url)
        .then(response => {
          return response.json();
        })
        .then(myJson => {
          this.setState({ data: myJson.groups.group });
        });
      console.log(this.state.page);
    }
  };
  change = e => {
    this.setState({ input: e.target.value });
  };
  search = () => {
    this.setState({ page: 1 });
    const url =
      "https://api.flickr.com/services/rest/?&method=flickr.groups.search&api_key=d5664b7cbf1eb574ebfa9b774697210a&text=" +
      this.state.input +
      "&per_page=" +
      this.state.perPage +
      "&page=" +
      this.state.page +
      "&format=json&nojsoncallback=1";
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        this.setState({ data: myJson.groups.group });
      });
    this.setState({ nextPageStyle: { visibility: "visible" } });
  };
  render() {
    return (
      <div>
        <Header />
        <SearchBar
          search={this.search}
          change={this.change}
          input={this.state.input}
        />
        <Nextpage
          prevNextPage={this.prevNextPage}
          perPage={this.state.perPage}
          page={this.state.page}
          nextPageStyle={this.state.nextPageStyle}
        />
        <div className="display">
          {this.state.data.map(group => (
            <Display group={group} />
          ))}
        </div>
      </div>
    );
  }
}

export default Flickr;
